import React from 'react';
import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Switch
} from "react-router-dom";
import WeatherPage from '../pages/WeatherPage';

export default function RootRoute() {

    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Redirect to="/copenhagen" />
                </Route>
                <Route path="/:city" component={WeatherPage} />
            </Switch>
        </Router>
    )
}