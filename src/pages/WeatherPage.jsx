import React, { useEffect, useState } from 'react';
import { useParams, useHistory  } from "react-router-dom";
import Header from '../components/Header';
import DisplayData from '../components/DisplayData';
import SearchCity from '../components/SearchCity';
import "./WeatherPage.modules.scss"

export default function WeatherPage() {

  let history = useHistory();
  const { city } = useParams();
  const [weatherData, setWeatherData] = useState({});

  useEffect(() => {
    const API_KEY = process.env.REACT_APP_WEATHER_API_KEY;
    const getWeatherData = () => {
      let fetchUrl = "http://api.openweathermap.org/data/2.5/weather?q=" + city + ",dk&appid=" + API_KEY;
      fetch(fetchUrl)
        .then(res => res.json())
        .then(
          (result) => {
            const newObject = {
              temp: result.main.temp,
              humidity: result.main.humidity,
              speed: result.wind.speed,
              deg: result.wind.deg
            }
            setWeatherData(newObject);    
          },
          (error) => {
            console.log(error);
          }
        )
    };

    getWeatherData();
  }, [city]);

  const handleSubmit = (event) => {
    event.preventDefault();
    const newCity = event.target.city.value;
    history.push("/" + newCity);
    event.target.reset();
  };

  return (
    <div className="widget" >
      <div className="panel panel-info">
        <Header city={city} />
        <ul className="list-group">
          <DisplayData weatherData={weatherData} />
          <SearchCity handleSubmit={handleSubmit} />
        </ul>
      </div>
    </div>
  );
}
