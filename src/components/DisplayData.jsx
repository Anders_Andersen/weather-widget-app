import PropTypes from 'prop-types';
import React, { Fragment } from 'react';

export default function DisplayData(props) {

  const { weatherData } = props;
  const { temp, humidity, speed, deg } = weatherData;

  const calculateFahrenheitToCelsius = () => {
      return (temp - 273.15).toFixed(2);
  }

  const windString = (d) => {
    const directions = ["N","NØ","Ø","SØ","S","SV","V","NV","N"];
    
    d += 22.5;
    if (d < 0) {
      d = 360 - Math.abs(d) % 360;
    } else { 
      d = d % 360;
    } 

    let w = parseInt(d / 45);
    return speed + " m/s " + directions[w];
  }

  return (
    <Fragment>
      <li className="list-group-item" id="temperature">Temperature: <b>{calculateFahrenheitToCelsius()}°C</b></li>
      <li className="list-group-item" id="humidity">Humidity: <b>{humidity}</b></li>
      <li className="list-group-item" id="wind">Wind: <b>{windString(deg)}</b></li>
    </Fragment>
  );
}

DisplayData.propTypes = {
  weatherData: PropTypes.object.isRequired
};