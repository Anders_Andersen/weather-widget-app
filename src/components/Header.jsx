import PropTypes from 'prop-types';
import React from 'react';

export default function Header(props) {

  const { city } = props;
  const cityCapitalized = city.charAt(0).toUpperCase() + city.slice(1);

  return (
    <div className="panel-heading" id="header">Weather in <b>{cityCapitalized}</b></div>
  );
}

Header.propTypes = {
  city: PropTypes.string.isRequired
};