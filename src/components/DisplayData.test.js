import { getAllByText, getByAltText, getByDisplayValue, getByTestId } from "@testing-library/react";
import React from "react";
import DisplayData from "./DisplayData";


describe("component: DisplayData", () => {
    let component;

    const weatherData = {
        temp: 300,
        humidity: 60,
        speed: 5,
        deg: 300,
    };

    beforeEach(() => {
        component = shallow(<DisplayData weatherData={weatherData} />)
    });

    it("should match snapshot", () => {
        expect(component).toMatchSnapshot();
    });

    it("should have correct data displayed", () => {
        const tempElement = component.find("#temperature");
        const humidityElement = component.find('#humidity');
        const windElement = component.find('#wind');

        expect(tempElement.text()).toContain("26.85°C");
        expect(humidityElement.text()).toContain("60");
        expect(windElement.text()).toContain("5 m/s NV");
    });

});