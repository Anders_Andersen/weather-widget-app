import React from "react";
import SearchCity from "./SearchCity";


describe("component: SearchCity", () => {
    let component;
    let handleSubmit;


    beforeEach(() => {
        handleSubmit = jest.fn();
        component = shallow(<SearchCity handleSubmit={handleSubmit} />)
    });

    it("should match snapshot", () => {
        expect(component).toMatchSnapshot();
    });

});