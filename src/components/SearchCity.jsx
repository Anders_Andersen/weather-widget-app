import PropTypes from 'prop-types';
import React, { Fragment } from 'react';


export default function SearchCity(props) {
  const { handleSubmit } = props;

  return (
    <Fragment>
      <li className="list-group-item">
        <form className="form-inline" onSubmit={handleSubmit}>
          <div className="form-group">
            <input type="text" className="form-control" id="city" placeholder="City"></input>
          </div>
          <button type="submit" className="btn btn-default">Search</button>
        </form>
      </li>
    </Fragment>
  );
}

SearchCity.propTypes = {
  handleSubmit: PropTypes.func.isRequired
};