import React from "react";
import Header from "./Header";


describe("component: Header", () => {
    let component;

    const city = "Roskilde";

    beforeEach(() => {
        component = shallow(<Header city={city} />)
    });

    it("should match snapshot", () => {
        expect(component).toMatchSnapshot();
    });

    it("should have correct data displayed", () => {
        const cityElement = component.find("#header");
        expect(cityElement.text()).toContain("Roskilde");
    });

});