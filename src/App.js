import React from 'react';
import RootRoute from './routes/RootRoute';

export default function App() {
  return (
    <div className="App">
        <RootRoute />
    </div>
  );
}
