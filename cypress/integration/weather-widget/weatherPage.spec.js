/// <reference types="cypress" />

context('weatherPage', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000')
  })


  it('should verify initial location', () => {
    cy.url().should('eq', 'http://localhost:3000/copenhagen')
  })

  it('should change location', () => {
    // given
    cy.url().should('eq', 'http://localhost:3000/copenhagen')
    
    // when  
    cy.get('input#city')
      .type('roskilde', { force: true })
      .should('have.value', 'roskilde')
    cy.get('button').click()

    // then
    cy.url().should('eq', 'http://localhost:3000/roskilde')
  })

  it('should have updated weather data', () => {

    // given
    cy.get('#header').should('contain.text', 'Copenhagen');
    const tempText = cy.get('#temperature');
    const humidityText = cy.get('#humidity');
    const windText = cy.get('#wind');

    // when 
    cy.get('input#city')
      .type('roskilde', { force: true })
      .should('have.value', 'roskilde') 
    cy.get('button').click()

    // then
    cy.get('#header').should('contain.text', 'Roskilde');
    expect(cy.get('#temperature')).to.not.equal(tempText);
    expect(cy.get('#humidity')).to.not.equal(humidityText);
    expect(cy.get('#wind')).to.not.equal(windText);
  })

})
