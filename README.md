# Weather-Widget
https://bitbucket.org/Anders_Andersen/weather-widget-app

### API server key
make copy of .env-temp, rename it to .env and update REACT_APP_WEATHER_API_KEY

### `npm start`
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`
Runs unit tests

### `npm run cy:run -- --spec "cypress/integration/weather-widget/weatherPage.spec.js"`
Runs cypress tests in cli for weather-widget (app must be running)

### `npm run cy:open`
opens cypress GUI locally (app must be running)

### `npm run build`
Builds the app for production to the `build` folder.<br />

## Docker production
docker build --rm -f "Dockerfile" -t weatherwidgetapp:latest "."
docker run --rm -d  -p 80:80/tcp weatherwidgetapp:latest